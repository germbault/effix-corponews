import { Pub } from 'pub';
import React from 'react';
import { CustomerModel } from './model/customermodel';


interface Props { }
interface State {
    cmsPub: CustomerModel;
}

export class Ads extends React.Component<Props, State> {


    constructor(props: Props) {
        super(props);

        this.state = {
            cmsPub: new CustomerModel,
        };
    }

    public render() {

        return (
            <>
                <section className='section'>
                    <section className='flex-container bloc_page'>
                        <section className='ads'>
                            {<Pub />}
                        </section>
                        <section className='ads'>
                            {<Pub />}
                        </section>
                        <section className='ads'>
                            {<Pub />}
                        </section>
                    </section>
                </section>{' '}
            </>
        );
    }

}
