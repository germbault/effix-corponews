import { WORDPRESS_API } from 'config.json';

export class ApiWp {
    private header = WORDPRESS_API;

    constructor(header?: string) {
        if (header) {
            this.header = WORDPRESS_API + header;
        }
    }

    public async getJson(url: string) {
        const result = await fetch(this.header + url, {
            method: 'GET',
            mode: 'cors',
            headers: {
                'content-type': 'application/json'
            },
            credentials: 'include'
        });
        await this.validateResult(result);
        return result.json();
    }

    protected notify(statusCode: number, message: string) {
        console.error(`${statusCode}: ${message}`);
    }

    private async validateResult(result: Response) {
        if (!result.ok) {
            this.notify(result.status, await result.text());
            throw new Error;
        }
    }
}
