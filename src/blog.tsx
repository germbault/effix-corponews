import { ApiWp } from 'apiwp';
import { WpModel } from 'model/wpmodel';
import React from 'react';


interface Props { }
interface State {
    articles: WpModel[];
}

export class Blog extends React.Component<Props, State> {
    private apiWp = new ApiWp;

    constructor(props: Props) {
        super(props);

        this.state = {
            articles: [],
        };
    }

    public async componentDidMount() {
        // "WORDPRESS_API": "https://vertlette.germain.devwebgarneau.com" 82
        // "WORDPRESS_API": "https://effix-commerce.bianka.devwebgarneau.com" 3

        const wpPosts = (await this.apiWp.getJson('/wp-json/wp/v2/posts?per_page=15') as any[]).map(WpModel.fromJSON);
        wpPosts.map(async post => {
            const postCatetories = post.categories.toString();
            if (post.id > 0 && postCatetories === '20') {
                const id = post.id;
                const title = post.title.rendered;
                let content = post.content.rendered;
                const categories = parseInt(post.categories.toString());
                const featured_media = post.featured_media;

                content = content.replace(/(<([^>]+)>)/gi, '');

                const wpPostsMedia = (await this.apiWp.getJson('/wp-json/wp/v2/media/' + post.featured_media) as WpModel);
                const source_url = wpPostsMedia.source_url;

                const articlesInfo = {
                    id: id,
                    title: title,
                    content: content,
                    categories: categories,
                    featured_media: featured_media,
                    source_url: source_url,
                };

                this.state.articles.push(articlesInfo);
                this.setState({ articles: this.state.articles });

            } else {
                console.log('Aucun article a été trouvé ou pas la bonne catégorie!');
            }
        });
    }

    public render() {
        const { articles } = this.state;
        // console.log(articles);

        return (
            <>
                <section className='flex-row bloc_page'>
                    <section className='flex-item'>
                        {!articles ? 'Chargement...' : articles.map(article =>
                            < div key={article.id} className='width50'>
                                <section className='container'>
                                    <h3>
                                        <a>
                                            {article.title}
                                        </a>
                                    </h3>
                                    <img className='image'
                                        src={article.source_url}
                                        alt='new'
                                    />
                                    <div className='middle'>
                                        <p>{article.content}</p>
                                    </div>
                                </section>
                            </div>
                        )}
                    </ section>
                </section>
            </>
        );
    }

}
