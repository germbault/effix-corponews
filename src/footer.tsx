import React from 'react';

export class Footer extends React.Component<{}> {
    public render() {
        return (
            <>
                <section className='flex-row'>
                    <section className='footer'>
                        <button className='button'><a href='https://germain.devwebgarneau.com/effix-ecommerce/' target='blank'>S'inscrire à l'info lettre</a></button>
                    </section>
                </section>
            </>
        );
    }
}
