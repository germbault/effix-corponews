import React from 'react';

export class Menu extends React.Component<{}> {
    public render() {
        return (
            <>
                <nav className='flex-row bloc_page'>
                    <section>
                        <a href='https://germain.devwebgarneau.com/effix-ecommerce/' target='blank'><img src='/img/fleche_retour.png' /></a>
                    </section>
                    <section>
                        <a href='http://effix-landone.germain.devwebgarneau.com/' target='blank'><img src='../img/logo_effix_blanc.png' /></a>
                    </section>
                </nav>
            </>
        );
    }
}
