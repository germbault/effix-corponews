// import {Router} from 'data/router';
import { Ads } from 'ads';
import { Blog } from 'blog';
import { Footer } from 'footer';
import { Menu } from 'header';
import { Hero } from 'hero';
import React from 'react';
import ReactDom from 'react-dom';
import { AdsSecond } from 'secondads';

ReactDom.render(
    <>
        {/* <section className='bloc_page'> */}
        <Menu />
        <Hero />
        <Ads />
        <Blog />
        <AdsSecond />
        <Footer />
        {/* </section> */}
    </>,
    document.getElementById('coreContainer')
);
