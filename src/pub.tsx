import { CustomerModel } from 'model/customermodel';
import React from 'react';


interface Props { }
interface State {
    pub: CustomerModel;
}

export class Pub extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            pub: new CustomerModel
        };
    }

    public async componentDidMount() {
        const axios = require('axios').default;

        await axios.get('http://localhost:1280/customer/pubrandom', {
            Headers: { 'content-type': 'application/json' }
        }).then((res: { data: any; }) => {

            this.setState({ pub: res.data });
        }).catch((error: any) => {
            console.log(error);
        });
    }

    public render() {


        return (
            <>
                <h1>{this.state.pub.username}</h1>
                <a href={this.state.pub.customer_site} target='blank'> <p>{this.state.pub.customer_site}</p> </a>
                <p>{this.state.pub.text}</p>
            </>
        );
    }



}
