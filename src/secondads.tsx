import { Pub } from 'pub';
import React from 'react';
import { CustomerModel } from './model/customermodel';


interface Props { }
interface State {
    cmsPub: CustomerModel;
}
export class AdsSecond extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            cmsPub: new CustomerModel,
        };
    }

    public render() {
        return (
            <>
                <section className='wrap'>
                    <section className='section'>
                        <section className='flex-container bloc_page'>
                            <section className='ads-second'>
                                {<Pub />}
                            </section>
                            <section className='ads-second'>
                                {<Pub />}
                            </section>
                            <section className='ads-second'>
                                {<Pub />}
                            </section>
                            <section className='ads-second'>
                                {<Pub />}
                            </section>
                            <section className='ads-second'>
                                {<Pub />}
                            </section>
                        </section>
                    </section>

                </section>
                {' '}
            </>
        );
    }
}
